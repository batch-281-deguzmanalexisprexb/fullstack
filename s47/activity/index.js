// console.log("Hello World!");
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

const updateFullName = () => {
	const fullName = `${txtFirstName.value} ${txtLastName.value}`
	spanFullName.innerHTML = fullName.trim();
};

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);
