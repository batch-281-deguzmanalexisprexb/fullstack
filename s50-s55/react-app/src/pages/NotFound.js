import { Link } from 'react-router-dom';

export default function NotFound() {
	return(
	<div>
		<h1>404 Not Found</h1>
		<p>Go back to the <Link to="/">homepage</Link></p>
	</div>
	)
}
	
