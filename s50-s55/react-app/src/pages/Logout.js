//it will allows us to navigate from one page of our application to another page
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import { useEffect, useContext} from 'react'

export default function Logout(){
	//It will allow us to clear all the information in the localStorage ensuring no information is stored in our browser
	// localStorage.clear();

	const { unsetUser, setUser } = useContext(UserContext);

	useEffect(() => {
		// The value from our localStorage will be clear
		unsetUser()

		// Since we cleared the contents of our localStorage using the unsetUser() therefore the value of our user state will be null;
		// By adding useEffect, this will allow the Logout page to render first before triggreing the useEffect which changes the state of our user
		setUser({
			id: null,
			isAdmin: null
		});
	}, [])

	return (
		<Navigate to = '/login'/>

	)
}